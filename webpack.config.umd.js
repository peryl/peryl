const path = require('path');
const webpack = require('webpack');
const glob = require("glob");
// const pkg = require('./package.json');

const entries = {
    ...glob.sync('./src/*.ts')
        .reduce(
            (entries, entry) =>
                Object.assign(entries,
                    { [entry.replace('src/', '').replace('.ts', '')]: './' + entry }),
            {})
};
console.log("entries:", entries);

const conf = {
    mode: 'production',

    // devtool: false,
    // devtool: "eval",
    devtool: "source-map",
    // devtool: "inline-source-map",
    // devtool: "eval-source-map",
    // devtool: "cheap-source-map",
    // devtool: "inline-cheap-source-map",
    // devtool: "cheap-module-source-map",
    // devtool: "cheap-eval-source-map",
    // devtool: "hidden-source-map",
    // devtool: "nosources-source-map",

    // entry: {
    //     index: "./src/index.ts"
    // },
    entry: entries,

    output: {
        // library: pkg.name,
        // libraryTarget: 'global',
        libraryTarget: 'umd',
        // filename: '[name].[chunkhash].js',
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/browser-umd')
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },

    plugins: [
        new webpack.ProgressPlugin(),
    ],

    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    }
};

module.exports = [conf];
