
export function zip<A, B>(a: A[], b: B[]): [A, B][] {
    return a.map((k, i) => [k, b[i]]);
}

export function transpose(matrix: any[][]): any[][] {
    return matrix[0].map((_, i) => matrix.map(row => row[i]));
}

// Test: npx ts-node src/zip-transpose.ts

// const a = ["a", "b"];
// const b = [1, 2];

// const z = zip(a, b);
// console.log(z);

// const m = [a, b];
// const t = transpose(m);
// console.log(t);
