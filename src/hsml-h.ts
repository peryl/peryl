import { HElement, HTagAttrs, HTagHead, HFnc, HTagChildren } from "./hsml";

export function h<ActionType extends string>(tag: HTagHead): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, text: string | number): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, children: HTagChildren<ActionType>): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, fnc: HFnc): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, attrs: HTagAttrs<ActionType>): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, attrs: HTagAttrs<ActionType>, text: string | number): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, attrs: HTagAttrs<ActionType>, children: HElement<ActionType>[]): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, attrs: HTagAttrs<ActionType>, fnc: HFnc): HElement<ActionType>;
export function h<ActionType extends string>(tag: HTagHead, second?: HTagAttrs<ActionType> | HTagChildren<ActionType>, third?: HTagAttrs<ActionType> | HTagChildren<ActionType>): HElement<ActionType> {
    const result: [string, ...any[]] = [tag];
    if (second) {
        if (typeof second === "string" || typeof second === "number") {
            result.push([second]);
        } else if (Array.isArray(second) || typeof second === "function") {
            result.push(second);
        } else {
            // element has attributes
            result.push(second);
        }
    }
    if (third) {
        if (typeof third === "string" || typeof third === "number") {
            result.push([third]);
        } else if (Array.isArray(third) || typeof third === "function") {
            result.push(third);
        }
    }
    return result as HElement<ActionType>;
}
