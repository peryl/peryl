//  Copyright 2018 The Incremental DOM Authors. All Rights Reserved.
/** @license SPDX-License-Identifier: Apache-2.0 */

export {applyAttr, applyProp, attributes, createAttributeMap} from './attributes';
export {alignWithDOM, alwaysDiffAttributes, close, createPatchInner, createPatchOuter, currentElement, currentContext, currentPointer, open, patchInner as patch, patchInner, patchOuter, skip, skipNode, tryGetCurrentElement} from './core';
export {setKeyAttributeName} from './global';
export {clearCache, getKey, importNode, isDataInitialized} from './node_data';
export {notifications} from './notifications';
export {symbols} from './symbols';
export {applyAttrs, applyStatics, attr, elementClose, elementOpen, elementOpenEnd, elementOpenStart, elementVoid, key, text} from './virtual_elements';
export * from './types';
