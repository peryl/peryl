// HSML tag head format "tag#id.class1.class2~handler"

export type HTagHeadName = keyof HTMLElementTagNameMap | `${string}${"-"}${string}`;
export type HTagHeadAttr = "." | "#" | "~";

export type HTagHead<T extends string = string> = `${HTagHeadName}` | `${HTagHeadName}${HTagHeadAttr}${T}`;

export type HAttrClasses = Array<string | [string, boolean]>;

// export type HAttrStyles = { [key: string]: string };
export type HAttrStyles = Partial<CSSStyleDeclaration>;

export type HAttrData = {
    [key: string]:
        | string
        | String
        | number
        | Number
        | boolean
        | Boolean
        | Date
        | Array<any>
        | Object;
};

export type HAttrOnDataFnc = (e: Event) => any;

export type HAttrOnData =
    | string
    | String
    | number
    | Number
    | boolean
    | Boolean
    | Date
    | Array<any>
    | Object
    | HAttrOnDataFnc
    | null;

export type HAttrOnCb = [keyof HTMLElementEventMap, EventListener];

export type HAttrOnAct<HAttrOnActType extends string> = [
    keyof HTMLElementEventMap | string,
    HAttrOnActType,
    HAttrOnData?
];

export type HAttrOn<HAttrOnActType extends string> = HAttrOnCb | HAttrOnAct<HAttrOnActType> | Array<HAttrOnCb | HAttrOnAct<HAttrOnActType>>;

export interface HTagAttrs<HAttrOnActType extends string> {
    readonly _id?: string;
    readonly _classes?: string[];
    readonly _ref?: string;
    readonly _hObj?: HObj<HAttrOnActType>;
    readonly key?: string;
    readonly skip?: boolean;
    readonly id?: string;
    readonly ref?: string;
    readonly classes?: HAttrClasses;
    readonly class?: string;
    readonly data?: HAttrData;
    readonly styles?: HAttrStyles;
    readonly style?: string;
    /** Event mapping to action, on: [event, action_type, action_data] */
    readonly on?: HAttrOn<HAttrOnActType>;
    /** Custom validation error message that is displayed when a form is submitted. */
    readonly validation?: {
        badInput?: string;
        patternMismatch?: string;
        rangeOverflow?: string;
        rangeUnderflow?: string;
        stepMismatch?: string;
        tooLong?: string;
        tooShort?: string;
        typeMismatch?: string;
        valueMissing?: string;
    };
    // HTMLElement
    readonly title?: string;
    readonly lang?: string;
    readonly accessKey?: string;
    readonly href?: string;
    readonly target?: "_blank" | "_self" | "_parent" | "_top";
    // HTMLFormElement
    readonly novalidate?: boolean | Boolean;
    // HTMLInputElement
    readonly type?:
        // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#input_types
        | "text"
        | "hidden"
        | "password"
        | "email"
        | "number"
        | "search"
        | "url"
        | "tel"
        | "color"
        | "date"
        | "datetime-local"
        | "month"
        | "range"
        | "time"
        | "week"
        | "submit"
        | "button"
        | "radio"
        | "checkbox"
        | "file"
        | "image"
        | "reset"
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
        | "text/plain"
        | "text/html"
        | "text/css"
        | "text/javascript" // (.js)"
        | "text/csv"
        | "text/xml"
        | "image/png"
        | "image/jpeg" // (.jpg, .jpeg, .jfif, .pjpeg, .pjp)"
        | "image/svg+xml" // (.svg)"
        | "audio/mp3"
        | "audio/ogg"
        | "video/mp4"
        | "video/ogg"
        | "application/json"
        // | "application/ld+json" // (JSON-LD)
        // | "application/msword" // (.doc)
        | "application/pdf"
        // | "application/sql"
        // | "application/vnd.api+json"
        // | "application/vnd.ms-excel" // (.xls)
        // | "application/vnd.ms-powerpoint" // (.ppt)"
        // | "application/vnd.oasis.opendocument.text" // (.odt)"
        // | "application/vnd.oasis.opendocument.spreadsheet" // (.ods)"
        // | "application/vnd.oasis.opendocument.presentation" // (.odp)"
        // | "application/vnd.openxmlformats-officedocument.presentationml.presentation" // (.pptx)"
        // | "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" // (.xlsx)"
        // | "application/vnd.openxmlformats-officedocument.wordprocessingml.document" // (.docx)"
        | "application/x-www-form-urlencoded"
        | "application/xml"
        // | "application/zip"
        // | "application/gzip"
        | "multipart/form-data"
        // | `text/${string}`
        // | `image/${string}`
        // | `audio/${string}`
        // | `video/${string}`
        // | `application/${string}`
        // | `message/${string}`
        // | `multipart/${string}`
        // | `${string}/${string}`
        // | string
        ;
    readonly name?: string;
    readonly value?: string | number | Number | String | null;
    readonly autocomplete?: string;
    readonly checked?: boolean | Boolean;
    readonly disabled?: boolean | Boolean;
    readonly list?: string;
    readonly max?: number | string;
    readonly maxlength?: number | string;
    readonly min?: number | string;
    readonly minlength?: number | string;
    readonly multiple?: boolean | Boolean;
    readonly pattern?: string;
    readonly placeholder?: string;
    readonly readonly?: boolean | Boolean;
    readonly required?: boolean | Boolean;
    readonly size?: number | string;
    readonly step?: number | string;
    // HTMLImageElement
    readonly src?: string;
    readonly alt?: string;
    readonly height?: number | string;
    readonly width?: number | string;
    // HTMLInputElement type file custom attributes
    readonly maxsize?: number;
    readonly convert?: "text" | "json" | "base64" | "object" | "dataurl" | "arraybuffer" | "stream";
    // Generic attributes
    readonly [key: string]:
        | string
        | String
        | string[]
        | String[]
        | number
        | Number
        | boolean
        | Boolean
        | Date
        | HAttrClasses
        | HAttrStyles
        // | HsmlAttrData
        | HAttrOn<HAttrOnActType>
        | EventListener
        | HObj<HAttrOnActType>
        | null
        | undefined;
}

export type HFnc = (e: Element) => boolean | void;

export interface HObj<HAttrOnActType extends string> {
    toHsml?(): HElement<HAttrOnActType>;
}

export interface HElements<HAttrOnActType extends string> extends Array<HElement<HAttrOnActType>> {}

export type HTagChildren<HAttrOnActType extends string> =
    | HElements<HAttrOnActType>
    | HFnc
    | HObj<HAttrOnActType>
    | string
    | String
    | boolean
    | Boolean
    | number
    | Number
    | Date
    | undefined
    | null;

export type HTagNoAttr<HAttrOnActType extends string> = [HTagHead, HTagChildren<HAttrOnActType>?];
export type HTagWithAttr<HAttrOnActType extends string> = [HTagHead, HTagAttrs<HAttrOnActType>, HTagChildren<HAttrOnActType>?];

export type HTag<HAttrOnActType extends string> = HTagNoAttr<HAttrOnActType> | HTagWithAttr<HAttrOnActType>;

export type HElement<HAttrOnActType extends string> =
    | HFnc
    | HObj<HAttrOnActType>
    | HTag<HAttrOnActType>
    | string
    | String
    | boolean
    | Boolean
    | number
    | Number
    | Date
    | undefined
    | null;

export interface HHandlerCtx<HAttrOnActType extends string> extends HObj<HAttrOnActType> {
    refs: { [name: string]: Element };
    actionCb(action: HAttrOnActType, data: HAttrOnData, e: Event): void;
}

export interface HHandler<HAttrOnActType extends string, C extends HHandlerCtx<HAttrOnActType>> {
    open(tag: HTagHeadName, attrs: HTagAttrs<HAttrOnActType>, children: HElements<HAttrOnActType>, ctx?: C): boolean;
    close(tag: HTagHeadName, children: HElements<HAttrOnActType>, ctx?: C): void;
    text(text: string, ctx?: C): void;
    fnc(fnc: HFnc, ctx?: C): void;
    obj(obj: HObj<HAttrOnActType>, ctx?: C): void;
}

/**  NO-BREAK SPACE */
export const NBSP = "\u00A0";

/**  NARROW NO-BREAK SPACE */
export const NNBSP = "\u202F";

/** THIN SPACE (&thinsp) */
export const THSP = "\u2009";

/** NUM/FIGURE SPACE (&numsp) */
export const NUMSP = "\u2007";


export function hsml<HAttrOnActType extends string, C extends HHandlerCtx<HAttrOnActType>>(
        hml: HElement<HAttrOnActType>,
        handler: HHandler<HAttrOnActType, C>,
        ctx?: C): void {
    // console.log("hsml", hsml);
    if (hml === undefined || hml === null) {
        return;
    }
    switch (hml.constructor) {
        case Array:
            // const tag = hml as HTag;
            // if (
            //     (
            //         tag.length === 1 &&
            //         tag[0].constructor === String
            //     ) ||
            //     (
            //         tag.length === 2 &&
            //         (
            //             tag[0].constructor === String &&
            //             (tag[1]!.constructor === Array || tag[1]!.constructor === Function)
            //         ) ||
            //         (
            //             tag[0].constructor === String &&
            //             tag[1]!.constructor === Object
            //         )
            //     ) ||
            //     (
            //         tag.length === 3 &&
            //         tag[0].constructor === String &&
            //         tag[1].constructor === Object &&
            //         tag[2]!.constructor === Array
            //     )
            // ) {
            //     hsmlTag(hml as HTag, handler, ctx);
            // } else {
            //     console.error("hsml parse error:", hml);
            //     // console.error("hsml parse error:", JSON.stringify(hml, null, 4));
            //     // throw Error(`hsml parse error: ${JSON.stringify(hml)}`);
            // }
            hsmlTag(hml as HTag<HAttrOnActType>, handler, ctx);
            break;
        case Function:
            handler.fnc(hml as HFnc, ctx);
            break;
        case String:
            handler.text(hml as string, ctx);
            break;
        case Boolean:
            handler.text("" + hml, ctx);
            break;
        case Number:
            const n = hml as number;
            const ns = n.toLocaleString ? n.toLocaleString() : n.toString();
            handler.text(ns, ctx);
            break;
        case Date:
            const d = hml as Date;
            const ds = d.toLocaleString ? d.toLocaleString() : d.toString();
            handler.text(ds, ctx);
            break;
        default: // HObj
            handler.obj(hml as HObj<HAttrOnActType>, ctx);
    }

    function hsmlTag(hmlTag: HTag<HAttrOnActType>, handler: HHandler<HAttrOnActType, C>, ctx?: C): void {
        // console.log("hsml tag", hmlTag);

        if (typeof hmlTag[0] !== "string") {
            console.error("HSML tag head not string:", hmlTag);
            return;
        }

        const head = hmlTag[0] as HTagHead;
        const attrsObj = hmlTag[1] as any;
        const hasAttrs = attrsObj && attrsObj.constructor === Object;
        const childIdx = hasAttrs ? 2 : 1;

        let children: HElements<HAttrOnActType> = [];
        let hFnc: HFnc | undefined;
        let hObj: HObj<HAttrOnActType> | undefined;

        const htc = hmlTag[childIdx];
        switch (htc != null && htc.constructor) { // switch ((typeof htc !== "undefined" || htc !== null) && htc.constructor) {
            case Array:
                children = htc as HElements<HAttrOnActType>;
                break;
            case Function:
                hFnc = htc as HFnc;
                break;
            case String:
            case Boolean:
            case Number:
            case Date:
                children = [htc as string | boolean | number | Date];
                break;
            default: // HObj
                hObj = htc as HObj<HAttrOnActType>;
                break;
        }

        const refSplit = head.split("~");
        const ref = refSplit[1];
        const dotSplit = refSplit[0].split(".");
        const hashSplit = dotSplit[0].split("#");
        const tag = (hashSplit[0] ?? "div") as HTagHeadName;
        const id = hashSplit[1];
        const classes = dotSplit.slice(1);

        let attrs: HTagAttrs<HAttrOnActType>;
        if (hasAttrs) {
            attrs = attrsObj as HTagAttrs<HAttrOnActType>;
        } else {
            attrs = {} as HTagAttrs<HAttrOnActType>;
        }

        if (id) {
            (attrs as any)._id = id;
        }
        if (classes.length) {
            (attrs as any)._classes = classes;
        }
        if (ref) {
            (attrs as any)._ref = ref;
        }
        if (hObj) {
            (attrs as any)._hObj = hObj;
        }

        const skip = handler.open(tag, attrs, children, ctx);

        if (hFnc) {
            handler.fnc(hFnc, ctx);
        }

        if (!skip) {
            children.forEach(jml => hsml(jml, handler, ctx));
        }

        handler.close(tag, children, ctx);
    }
}

export function hjoin<HAttrOnActType extends string>(hsmls: HElements<HAttrOnActType>, sep: string | HElement<HAttrOnActType>): HElements<HAttrOnActType> {
    const r = hsmls.reduce<HElements<HAttrOnActType>>(
        (p, c) => (p.push(c, sep), p),
        [] as HElements<HAttrOnActType>
    );
    r.splice(-1);
    return r;
}
