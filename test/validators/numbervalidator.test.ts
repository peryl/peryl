import "jasmine";
import { NumberValidator } from "../../src/validators";

describe("NumberValidator", () => {

    // required

    it("should test required option", () => {
        const sv = new NumberValidator({
            required: true
        });
        const result = sv.validate("0");
        expect(result.err).toEqual("");
    });

    it("should test required option with empty string [ERROR]", () => {
        const sv = new NumberValidator({
            required: true
        });
        const result = sv.validate("");
        expect(result.err).toEqual("required");
    });

    // min

    it("should test min 0 option with number 0", () => {
        const sv = new NumberValidator({
            min: 0
        });
        const result = sv.validate("0");
        expect(result.err).toEqual("");
    });

    it("should test min 0 option with number -1 [ERROR]", () => {
        const sv = new NumberValidator({
            min: 0
        });
        const result = sv.validate("-1");
        expect(result.err).toEqual("not_in_range");
    });

    // max

    it("should test max 10 option with number 5", () => {
        const sv = new NumberValidator({
            max: 10
        });
        const result = sv.validate("5");
        expect(result.err).toEqual("");
    });

    it("should test max 0 option with number 1 [ERROR]", () => {
        const sv = new NumberValidator({
            max: 0
        });
        const result = sv.validate("1");
        expect(result.err).toEqual("not_in_range");
    });

    // min & max

    it("should test <min3, max7> with number 3", () => {
        const sv = new NumberValidator({
            min: 3,
            max: 7
        });
        const result = sv.validate("7");
        expect(result.err).toEqual("");
    });

    it("should test <min3, max7> with number 5", () => {
        const sv = new NumberValidator({
            min: 3,
            max: 7
        });
        const result = sv.validate("5");
        expect(result.err).toEqual("");
    });

    it("should test <min3, max7> with number 7", () => {
        const sv = new NumberValidator({
            min: 3,
            max: 7
        });
        const result = sv.validate("");
        expect(result.err).toEqual("");
    });

    it("should test <min3, max7> with number 8 [ERROR]", () => {
        const sv = new NumberValidator({
            min: 3,
            max: 7
        });
        const result = sv.validate("8");
        expect(result.err).toEqual("not_in_range");
    });

    it("should test <min3, max7> with number 2 [ERROR]", () => {
        const sv = new NumberValidator({
            min: 3,
            max: 7
        });
        const result = sv.validate("2");
        expect(result.err).toEqual("not_in_range");
    });

});
