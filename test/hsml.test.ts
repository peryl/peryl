import "jasmine";
import { HElement, HElements } from "../src/hsml";

describe("hsml", () => {

    it("syntax check", () => {
        const hsmls: HElements<string> = [
            "text",
            ["tag-a", [
                "d",
                ["div"]
            ]],
            ["tag-b", { attr: "attr", classes: ["class"] }, [
                "text",
                123,
                true
            ]]
        ];
        // console.log(hsmls);

        const hml: HElement<string> = ["x-xx", {}, [
                "x-types", " ", 1235.456, " ", new Date(), " ",
                ...hsmls,
                ["i", ["t", "a", ""]],
                ["i", {}, ["t", "a", ""]],
                ["i"]
            ]];
        // console.log(hml);
        hml[0];

        const hml1: HElement<string> = ["div", {}, [
            ["span", {}, [
                ["a",
                    {
                        href: "https://gitlab.com/peter-rybar/diasheet",
                        title: "GitLab",
                        target: "_blank"
                    },
                    [["i.fa.fa-gitlab"]]
                ]
            ]],
            ["span"]
        ]];
        // console.log(hml1);
        hml1[0];

        const hml2: HElement<string> = ["div", [
            ["h2", ["title"]],
            ["div.w3-card-12", [
                ["header.w3-container w3-light-grey", [
                    ["h3", ["Account: ", "User"]]
                ]],
                ["div.w3-container.w3-light-grey", [
                    ["p", ["Balance: 4 DCT"]],
                    ["br"]
                ]],
                ["h4", ["Your account spending"]],
                ["div#piechart"],
                ["h4", ["Your account balance"]],
                ["div#linechart"],
                ["button.w3-button.w3-block.w3-dark-grey", ["Refresh"]]
            ]],
            ["br"]
        ]];
        // console.log(hml2);
        hml2[0];

        // const data = { attr: "action-data" };
    });

});
