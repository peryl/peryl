import "jasmine";
import { HElements } from "../src/hsml";
import { hsmls2htmls } from "../src/hsml-html";

describe("hsml-html", () => {

    it("rendering", () => {
        const dispatch = (action: string, data?: any, event?: Event): void => {
            console.log("action:", action, data, event);
        };
        const data = { attr: "action-data" };
        const hmls: HElements<string> = [
            ["button", { on: ["click", "action", data] }, "send"],
            ["h1", "aaa"],
            ["input",
                {
                    type: "text",
                    on: [
                        ["mouseover", "hover-action", data],
                        ["change", "click-action", e => (e.target as HTMLInputElement).value],
                        ["click", e => dispatch("action", data, e)],
                    ],
                    click: e => dispatch("action", data, e)
                }
            ],
            ["button",
                {
                    on: ["click", e => dispatch("action", data, e)],
                    click: e => dispatch("action", data, e)
                },
                ["Send"]
            ]
        ];
        const html = hsmls2htmls(hmls, true).join("");
        // console.log(html);
        expect(html).toEqual(
`<button>
    send
</button>
<h1>
    aaa
</h1>
<input type="text"/>
<button>
    Send
</button>
`);
    });

});
