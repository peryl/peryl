import * as hash from "../src/hash";
import { select } from "../src/dom";

const out = select("#output");
out && (out.innerHTML = "test");

const emitWritten = true;

interface Query {
    a: string;
    b: string;
    c: string;
    time: string;
    u?: string;
}

const h = new hash.Hash<hash.HashUrlData<keyof Query>>(emitWritten)
    // .coders(hash.jsonEncode, hash.jsonDecode)
    .coders(hash.hashUrlEncode, hash.hashUrlDecode)
    .onChange(data => {
        console.log("hash:", JSON.stringify(data));
        out && (out.innerHTML += "<br/>" + "hash: " + JSON.stringify(data));
    })
    .listen()
    // .write("hello");
    // .write({ message: "hello" });
    .write({
        path: ["hello", "peter", undefined],
        query: { a: "a?a", b: "b&b", c: " .%", u: undefined }
    });

(self as any).h = h;

const el = select("#hash");
// el && (el.onclick = (e: MouseEvent) => h.write(new Date().getTime()));
// el && (el.onclick = (e: MouseEvent) => h.write({ time: new Date().getTime(), x: " &?" }));
el && (el.onclick = (e: MouseEvent) => h.write({ path: ["hello", "pet/e r"], query: { a: ["aaá", "x/x x"], b: "b&b", c: " ?.%", time: "" + new Date().getTime() } }));
// el && (el.onclick = (e: MouseEvent) => h.write("hello/peter?a=aa%25C3%25A1&b=b%2526b&c=+%253F.%2525&time=" + new Date().getTime()));
// el && (el.onclick = (e: MouseEvent) => h.write("hello/peter"));
