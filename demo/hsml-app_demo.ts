import { HApp, HDispatcher, HState, HView } from "../src/hsml-app";

// Actions definition
// enum Actions {
//     say = "say",
//     fetch = "fetch"
// }

type Actions = "say" | "fetch";

interface State {
    message: string;
    json: string;
}

// App state definition and initialization
const state: HState<State> = function () {
    return {
        message: "",
        json: ""
    };
};

// Template function, returns HSML markup generated from app state
const view: HView<State, Actions> = function (state) {
    return [
        ["p", [
            "Greeting: ", state.message
        ]],
        ["p", [
            // On button event "click" dispatch Action.say type with data "Hello"
            ["button", { on: ["click", "say", "Hello"] }, "Say Hello"],
            " ",
            ["button", { on: ["click", "say", "Hi"] }, "Say Hi"],
        ]],
        ["p", [
            ["button", { on: ["click", "fetch"] }, "Server fetch time"],
            ["pre", state.json]
        ]]
    ];
};

// Action dispatcher, app logic
const dispatcher: HDispatcher<State, Actions> = async function (action, state, dispatch) {
    switch (action.type) {

        case "say":
            // Change app state message by action data (3. parameter of on click action)
            state.message = action.data;
            break;

        case "fetch":
            // Server async call
            try {
                const res = await fetch("http://date.jsontest.com");
                const data = await res.json();
                state.json = JSON.stringify(data, null, 4);
            } catch (error) {
                state.json = String(error);
            }
            break;
    }
};

// HApp.error = function () {
//     console.dir(arguments);
//     console.error.apply(undefined, arguments);
// };

// Run application
(window as any).app = new HApp<State, Actions>(state, view, dispatcher, "app", true);
