import { objPaths, objValuePaths } from "../src/objpaths";

const obj = {
    obj1: {
        obj2: {
            data1: 213,
            data2: "1231",
            obj3: {
                data: "milf"
            }
        }
    },
    obj4: {
        description: "toto",
        cougars: "Jodi",
        category: "milf"
    }
};

const vpaths = objValuePaths(obj);
const paths = objPaths(obj);
const strPaths = paths.map(p => p.join("."));

console.log(JSON.stringify(obj, null, 4));
console.log(JSON.stringify(vpaths, null, 4));
console.log(JSON.stringify(paths, null, 4));
console.log(JSON.stringify(strPaths, null, 4));
