import { HAction, HAppActions, happel, HDispatcher, HDispatchScopes, HState, HView } from "../src/hsml-app";

interface CounterState {
    count: number;
}

enum CounterActions {
    dec = "counter-dec",
    inc = "counter-inc",
    count = "counter-count"
}

const counterState: HState<CounterState> = function () {
    return {
        count: 77
    }
};

const counterView: HView<CounterState, CounterActions> = function (state) {
    return [
        ["h3", ["Counter"]],
        ["p", [
            ["em", ["Count"]], ": ", state.count,
            " ",
            ["button", { on: ["click", CounterActions.dec, 1] }, ["-"]],
            ["button", { on: ["click", CounterActions.inc, 2] }, ["+"]]
        ]]
    ];
};

const counterDispatcher: HDispatcher<CounterState, CounterActions> = async function (action, state, dispatch) {
    // console.log("action counter:", action);
    // console.log("state counter:", state);
    // console.log("happ counter:", this);

    switch (action.type) {
        case HAppActions.init:
        case HAppActions.mount:
        case HAppActions.umount:
            break;

        case HAppActions.attribute:
            if (action.data.attrName === "count") {
                state.count = Number(action.data.newVal);
            }
            break;

        case CounterActions.inc:
            state.count = state.count + action.data as number;
            setTimeout(() => dispatch(CounterActions.dec, 1), 1e3); // async call
            dispatch(CounterActions.count, state.count, HDispatchScopes.element);
            break;

        case CounterActions.dec:
            state.count = state.count - action.data as number;
            dispatch(CounterActions.count, state.count, HDispatchScopes.element);
            break;
    }
};

happel<CounterState, CounterActions>({
    state: counterState,
    view: counterView,
    dispatcher: counterDispatcher,
    id: "happ-counter",
    // attributes: ["count"],
    debug: true
});


interface AppState {
    count: number;
}

enum AppActions {
    action = "app-action"
}

const appState: HState<AppState> = function () {
    return {
        count: 33
    }
};

const appView: HView<AppState, AppActions> = function (state) {
    return [
        ["h2", ["App count: ", state.count]],
        ["happ-counter", {
            count: state.count,
            on: [HAppActions.action, AppActions.action]
        }]
    ];
};

const appDispatcher: HDispatcher<AppState, AppActions | CounterActions> = async function (action, state, dispatch) {
    // console.log("action:", action);
    // console.log("state:", state);
    // console.log("happ:", this);

    switch (action.type) {
        case HAppActions.attribute:
            if (action.data.attrName === "count") {
                state.count = Number(action.data.newVal);
            }
            break;
        case AppActions.action:
            const a = action.data as HAction<AppActions | CounterActions>;
            switch (a.type) {
                case CounterActions.count:
                    state.count = a.data as number;
                    dispatch(CounterActions.count, state.count, HDispatchScopes.element);
                    break;
                default:
                    console.log(a);
                    break;
            }
            break;
    }
};

happel<AppState, AppActions | CounterActions>({
    state: appState,
    view: appView,
    dispatcher: appDispatcher,
    id: "happ-app",
    // attributes: [],
    debug: true
});
