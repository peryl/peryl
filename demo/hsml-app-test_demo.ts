import { HElement } from "../src/hsml";
import { HFormData } from "../src/hsml-app";
import { HAction, HApp, HAppActions, HDispatcher, HFormInputData, HState, HView, HView1 } from "../src/hsml-app";

interface CounterState {
    count: number;
}

enum CounterActions {
    dec = "counter-dec",
    inc = "counter-inc"
}

const counterView: HView1<CounterState, CounterActions> = (state) => {
    return ["div~reference", [
        ["h2", ["Counter"]],
        ["p", [
            ["em", ["Count"]], ": ", state.count,
            " ",
            ["button", { on: ["click", CounterActions.dec, 1] }, ["-"]],
            ["button", { on: ["click", CounterActions.inc, 2] }, ["+"]]
        ]]
    ]];
};

const counterDispatcher: HDispatcher<CounterState, CounterActions> = async function (this: HApp<CounterState, CounterActions>, action, state, dispatch) {
    console.log("action counter:", action);
    // console.log("state counter:", state);
    // console.log("happ counter:", this);

    switch (action.type) {

        case HAppActions.mount:
            this.windowActionListen();
            break;

        case HAppActions.action:
            const a = action.data as HAction<Actions>;
            switch (a.type) {
                case Actions.xXx:
                    dispatch(CounterActions.dec, a.data);
                    break;
            }
            break;

        case CounterActions.inc:
            state.count = state.count + action.data as number;
            setTimeout(() => dispatch(CounterActions.dec, 1), 1e3); // async call
            break;

        case CounterActions.dec:
            state.count = state.count - action.data as number;
            break;
    }
};

interface State {
    title: string;
    counter: CounterState;
    x?: boolean | Boolean;
}

enum Actions {
    title = "title",
    clear = "clear",
    formSubmit = "formSubmit",
    formChange = "formChange",
    xXx = "xXx",
    x = "x"
}

const state: HState<State> = function () {
    return {
        title: "Title",
        counter: {
            count: 77
        }
    };
};

const view: HView<State, Actions | CounterActions> = function (state) {
    return [
        ["h2", [state.title]],
        ["p", [
            "Title: ",
            ["input",
                {
                    type: "text",
                    // name: "title",
                    value: new String(state.title),
                    on: ["input", Actions.title]
                }
            ], " ",
            ["button.w3-button.w3-blue",
                {
                    type: "button",
                    on: ["click", Actions.clear]
                },
                ["Clear title"]
            ]
        ]],
        counterView(state.counter),
        ["h2", ["Form"]],
        ["form",
            {
                on: [
                    ["submit", Actions.formSubmit],
                    ["change", Actions.formChange]
                ]
            },
            [
                "xy ",
                ["input",
                    {
                        type: "text",
                        name: "xy",
                        value: "x"
                    }
                ],
                ["input",
                    {
                        type: "text",
                        name: "xy",
                        value: "y"
                    }
                ],
                ["br"],
                "a ",
                ["input", {type: "checkbox", name: "a"}],
                ["br"],
                "b ",
                ["input", {type: "checkbox", name: "b", value: "b"}],
                ["br"],
                "c ",
                ["input", {type: "checkbox", name: "c"}],
                ["input", {type: "checkbox", name: "c"}],
                ["br"],
                "d ",
                ["input", {type: "checkbox", name: "d", value: "d1"}],
                ["input", {type: "checkbox", name: "d", value: "d2"}],
                ["br"],
                "r ",
                ["input", {type: "radio", name: "r", value: "r1"}],
                ["input", {type: "radio", name: "r", value: "r2"}],
                ["br"],
                "s ",
                ["select", { name: "s" },
                    ["s1", "s2", "s3"]
                        .map<HElement<Actions>>(l => ["option", { value: l }, [l]])
                ],
                ["br"],
                "sm ",
                ["select", { name: "sm", multiple: true },
                    ["sm1", "sm2", "sm3"]
                        .map<HElement<Actions>>(l => ["option", { value: l }, [l]])
                ],
                ["br"],
                ["textarea", { name: "ta" },
                    "text area"
                ],
                ["br"],
                ["button.w3-button.w3-blue",
                    ["submit"]
                ]
            ]
        ],
        ["h2", ["Action event"]],
        ["p", [
            ["button", { on: ["click", Actions.xXx] }, ["xXx"]]
        ]],
        ["h2", ["Props update"]],
        ["input", { type: "checkbox", name: "x", on: ["change", Actions.x] }],
        ["input", { type: "checkbox", checked: state.x }],
        ["input", { type: "radio", name: "y", checked: state.x }],
        ["input", { type: "radio", name: "y", checked: !state.x }],
        ["input", { type: "button", value: "x", disabled: state.x }],
        ["div#id~ref", { id: "ID", ref: "REF" }],
        ["div#id~ref", { _id: "ID", _ref: "REF" }]
    ];
};

const dispatcher: HDispatcher<State, Actions | CounterActions> = async function (this, action, state, dispatch) {
    console.log("action:", action);
    // console.log("state:", state);
    // console.log("happ:", this);
    console.log("refs:", this.refs);

    counterDispatcher.call(this, action, state.counter, dispatch);

    switch (action.type) {
        case HAppActions.init:
        case HAppActions.mount:
        case HAppActions.umount:
            break;

        case Actions.title:
            state.title = (action.data as HFormInputData).value as string;
            break;

        case Actions.clear:
            state.title = "";
            break;

        case Actions.formSubmit:
            console.log("FormSubmit:", JSON.stringify(action.data as HFormData<any>));
            const fe = action.event!.target as HTMLFormElement;
            const fd = new FormData(fe);
            console.log("FormData:", JSON.stringify([...fd.entries()]));
            break;

        case Actions.formChange:
            console.log("FormChange:", JSON.stringify(action.data as HFormInputData));
            break;

        case Actions.xXx:
            dispatch(Actions.xXx, 1, "window");
            break;

        case Actions.x:
            console.log("data.x", typeof action.data.x, action.data.x);
            // state.x = new Boolean(action.data.x === "true");
            // state.x = action.data.x === "true";
            state.x = new Boolean(action.data.x);
            // state.x = action.data.x;
            break;

        // default:
        //     console.warn("action unhandled:", action);
    }
};

const app = new HApp<State, Actions | CounterActions>(state, view, dispatcher);
app.debug = window.location.hostname === "localhost";
app.mount(document.getElementById("app"));
(self as any).app = app;


// const html = app.toHtml();
// console.log(html);
