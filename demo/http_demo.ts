import { authBasic, authJwt, http } from "../src/http";

http.onRequest(req => {
    req.headers({ "X-Requested-With": "XMLHttpRequest" });
    console.log(req);
});

http.onResponse(res => {
    console.log(res);
});

http.onError(e => {
    console.log(e);
});

http.get("http://date.jsontest.com")
    // .use(authBasic("login", "passwd"))
    // .use(authJwt("JWT-auth-tocken"))
    .onProgress(progress => {
        console.log("progress: ", progress);
    })
    .onResponse(response => {
        console.log("response: " + response.getContentType(), response.getBody());
    })
    .onError(error => {
        console.log("response error: ", error);
    })
    .noCache()
    .send();

http.get("http://date.jsontest.com")
    .onProgress(progress => {
        console.log("progress: ", progress);
    })
    .noCache()
    .sendPromise()
    .then(response => {
        console.log("response: " + response.getContentType(), response.getBody());
    })
    .catch(error => {
        console.log("response error: ", error);
    });

// get("https://maps.googleapis.com/maps/api/geocode/json", {
//         sensor: false,
//         address: "Bratislava I",
//         xxx: ["yyy", "zzz"]
//     })
//     // .timeout(10)
//     .onProgress(progress => {
//         console.log("response progress: ", progress);
//     })
//     .onResponse(response => {
//         console.log("response: " + response.getContentType(), response.getJson());
//     })
//     .onError(error => {
//         console.log("response error: ", error);
//     })
//     .noCache()
//     // .headers({"Content-Type": "application/json"})
//     // .send(data, "application/json");
//     .send();

// dd if=/dev/urandom of=http_demp.bigfile bs=1M count=100
// get("http_demp.bigfile")
//     .onProgress(progress => {
//         console.log("progress: ", progress);
//     })
//     .onResponse(response => {
//         console.log("response: " + response.getContentType(), response);
//     })
//     .onError(error => {
//         console.log("response error: ", error);
//     })
//     .noCache()
//     .send();


// HttpRequest.abortAll();
