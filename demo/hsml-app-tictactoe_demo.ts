import { HElement, HElements, NBSP } from "../src/hsml";
import { HApp } from "../src/hsml-app";

const CIRC = "\u25EF";
const CROS = "\u2A2F";

interface State {
    board: string[][];
    turn: number;
}

const enum Actions {
    mark = "mark",
    reset = "reset",
    noop = "noop"
}

const boardInit = () => [
    [NBSP, NBSP, NBSP],
    [NBSP, NBSP, NBSP],
    [NBSP, NBSP, NBSP]
];

function state(): State {
    return {
        board: boardInit(),
        turn: 0
    };
}

function view(state: State): HElements<Actions> {
    return [
        ["div.w3-content", [
            ["h1", ["Tic-Tac-Toe Demo"]],
            ["div.w3-center", [
                ["p.w3-xlarge", [
                    "Player: ", state.turn ? CROS : CIRC,
                ]],
                ["div", state.board.map<HElement<Actions>>((row, y) =>
                    ["div", row.map<HElement<Actions>>((col, x) =>
                        ["button.w3-button.w3-white.w3-border.w3-border-gray",
                            {
                                styles: {
                                    fontFamily: "monospace",
                                    fontSize: "300%",
                                    display: "inline-block",
                                    width: "2em",
                                    height: "2em"
                                },
                                on: (col === NBSP)
                                    ? ["click", Actions.mark, { x, y, turn: state.turn }]
                                    : ["click", Actions.noop]
                            },
                            col
                        ])
                    ])
                ],
                ["p", [
                    ["button.w3-button.w3-blue", { on: ["click", Actions.reset] }, "Reset"]
                ]]
            ]]
        ]]
    ];
}


async function dispatcher(action, state, dispatch) {
    console.log("action", action);

    switch (action.type) {

        case Actions.reset:
            state.board = boardInit();
            break;

        case Actions.mark:
            state.board[action.data.y][action.data.x] = action.data.turn ? CROS : CIRC;
            state.turn = action.data.turn ? 0 : 1;
            break;
    }
}

const app =  new HApp<State, Actions>(state, view, dispatcher, "app");

(self as any).app = app;
